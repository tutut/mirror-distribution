import os, sys, subprocess, time, select, re, time, socket

min_memory = 100

def get_lock(process_name):
    global lock_socket   # Without this our lock gets garbage collected
    lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    try:
        lock_socket.bind('\0' + process_name)
        print("\nLock Monitoring Memory Process")
    except socket.error:
        print("\nLock Exsist")
        sys.exit()

#cmd = r"""ps uax | grep "/usr/local/bin/python3.5" | awk '{print "\"" $2 "\":\"" $11 "\""}'"""
cmd = r"""free -m | awk 'NR==2{print $4}'"""
listed = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
backend = listed.stdout.read()

bin = backend.replace(b'\n',b'')
ba = bin.decode("utf-8") 

def main():
	get_lock('running_app_mem_monitor')
	try:
		if int(ba) <= min_memory:
			print("\nLebih kecil")
			times = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
			print(times)
			cmd = r"""free -m"""
			listed = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
			freebin = listed.stdout.read()
			freestring = freebin.decode("utf-8")

			psuax = r"""ps uax | grep ./webio-replica"""
			listedps = subprocess.Popen(psuax, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
			psbin = listedps.stdout.read()
			psstring = psbin.decode("utf-8")
 
			message = freestring+"\n\n"+psstring
			print(message)
			sys.exit(1)
		elif int(ba) >= min_memory:
			print("\nLebih Besar")
			times = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
			print(times)
			cmd = r"""free -m"""
			listed = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
			freebin = listed.stdout.read()
			freestring = freebin.decode("utf-8")

			psuax = r"""ps uax | grep ./webio-replica"""
			listedps = subprocess.Popen(psuax, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
			psbin = listedps.stdout.read()
			psstring = psbin.decode("utf-8")

			message = freestring+"\n\n"+psstring
			print(message)
			sys.exit(1)
		else:
			pass

	except Exception:
		e = sys.exc_info()[0]
		#sendAlert(e,"App Memory Monitor")


if __name__ == "__main__":
  #Run as main program
  main()

