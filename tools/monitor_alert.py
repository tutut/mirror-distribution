#!/opt/python3/bin/python3.5

import os, subprocess, re, smtplib, socket, sys, time, multiprocessing, setproctitle, fileinput, re, requests
from fractions import Fraction
from email.mime.text import MIMEText

queue = multiprocessing.Queue()

### end dir not with / ###
path_app = "/opt/python3/app"

def get_lock(process_name):
    global lock_socket   # Without this our lock gets garbage collected
    lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    try:
        lock_socket.bind('\0' + process_name)
        print('lock_monitoring')
        sys.stdout.flush()
    except socket.error:
        print('lock_exists')
        sys.exit()

def sendAlert(pesan):
       s = smtplib.SMTP('{SMTP-ADDRESS}')
       msg = MIMEText(""" %s """ % pesan)
       sender = ''
       recipients = ['']
       msg['Subject'] = "Failure webdav mirror" 
       msg['From'] = sender
       msg['To'] = ", ".join(recipients)
       s.sendmail(sender, recipients, msg.as_string())

def handle_queue(queue,queue_host):
    setproctitle.setproctitle(sys.argv[0] + ' %s' % queue_host)

    while not queue.empty():
          data = queue.get()

          filepath = path_app+"/dummy_.xml"
          http = "http://"+data[0]+"/dummy_.xml"

          with open(filepath) as fh:
                   mydata = fh.read()
                   try:
                      response = requests.put(http,
                                          data=mydata,                         
                                          params={'file': filepath},
                                          timeout=3
                                          )
                      status_code = response.status_code
                   except Exception:
                      status_code = 503
          fh.closed 

          if status_code == 200 or status_code == 201 or status_code == 204:
             print("OK: "+data[0])
             sys.stdout.flush()
          else:
              print("ERROR: "+data[0])
              sys.stdout.flush()
              sendAlert("Monitoring webdav \n\n Gagal distribusi webdav: "+data[0]+"\n Status: "+str(status_code)+" \n\n\n\n Thanks Boot")


if __name__ == "__main__":
   jobs = []
   p = {}
   get_lock('check_mirror')
   
   try:
         with open(path_app+"/replicate_alert.conf") as f:
              lines = f.read().splitlines()

         for i in lines:
               try:
                      queue.put((i,i))
                      p[i] = {'queue_host': i ,'proc': multiprocessing.Process(target=handle_queue, args=(queue,i))}
                      jobs.append(p[i]['proc'])
                      p[i]['proc'].start()
               except Exception:
                   e = sys.exc_info()[0]
                   sendAlert(e)         

         time.sleep(30)
         for j in jobs:
               j.terminate()
               j.join()
   except Exception:
      print("""
            Not format running.
           """)

