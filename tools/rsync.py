#!/opt/python3/bin/python3.5

import os, subprocess, re, smtplib, socket, sys, time, multiprocessing, setproctitle, fileinput, re, requests
from fractions import Fraction
from email.mime.text import MIMEText

queue = multiprocessing.Queue()

### end log_dir with / ###
log_dir = "/var/log/webio/"

### end root_server not with / ###
root_server = "/backup"
path_app = "/opt/python3/app"

class Convert2Bytes(object):
    def __init__(self,val):
        self.val=val
    def convert2Bytes(self):
        Data = self.val
        multi = 1024 # In Byte
        if Data:
            if Data.find('KB') != -1:
                Data = Data.replace("KB","")
                Data = Fraction(Data)
                return (int(Data) * multi)
            elif Data.find('MB') != -1:
                Data = Data.replace("MB","")
                Data = Fraction(Data)
                return (int(Data) * multi * multi)
            elif Data.find('B') != -1:
                Data = Data.replace("B","")
                Data = Fraction(Data)
                return (int(Data))
        else:
            return 0

def get_lock(process_name):
    global lock_socket   # Without this our lock gets garbage collected
    lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    try:
        lock_socket.bind('\0' + process_name)
        print('lock_monitoring')
        sys.stdout.flush()
    except socket.error:
        print('lock_exists')
        sys.exit()

def sendAlert(pesan):
       cmd = "/usr/sbin/ifconfig |grep {IP}| awk '{print $2}'"
       getip = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
       ipnya = getip.stdout.read()

       s = smtplib.SMTP('{SMTP-ADDRESS}')
       msg = MIMEText(""" %s """ % pesan)
       sender = ''
       recipients = ['']
       msg['Subject'] = "Rsync minicapella IP: "+ ipnya.strip('\n') +" error" 
       msg['From'] = sender
       msg['To'] = ", ".join(recipients)
       s.sendmail(sender, recipients, msg.as_string())

def handle_queue(queue,queue_host):
    setproctitle.setproctitle(sys.argv[0] + ' %s' % queue_host)
    while not queue.empty():
          data = queue.get()
          status = False
          err_value = []

          with open(data[0], 'r') as infile:
             for line in infile:
                 line_strip = line.rstrip('\n')
                 list_dir = line_strip.split()
                 #print(list_dir)
                 #time.sleep(1)

                 try:

                    if line_strip == '':
                       print("Has been auto rsync "+data[0])
                       sys.stdout.flush()
                       status = True

                    else:
                       file_locations = list_dir[5:6][0]
                       method = list_dir[3:4][0]
                       host_file = None
                       if method == 'PUT':
                          response = None
                          http = "http://"+data[2]+"".join(file_locations)
                          host_file = root_server,file_locations
                          print("curl -XPUT "+"".join(host_file)+" "+http)
                          sys.stdout.flush()

                          #files = {'file': open("".join(host_file))}
                          filepath = "".join(host_file)
                          with open(filepath) as fh:
                               mydata = fh.read()
                               response = requests.put(http,
                                          data=mydata,                         
                                          #headers={'content-type':'text/plain'},
                                          params={'file': filepath}
                                          )
                          if response.status_code == 200 or response.status_code == 201 or response.status_code == 204:
                             status = True
                          else:
                             err_value.append(" ".join(list_dir))
                             status = False
                          
                       #elif method == 'DELETE':

                       #   http = "http://"+data[2]+"".join(file_locations)
                       #   host_file = root_server,file_locations
                       #   print("curl -XDELETE "+"".join(host_file)+" "+http)
                       #   sys.stdout.flush()

                          #response = requests.delete(http)

                       #   if response.status_code == 200 or response.status_code == 201 or response.status_code == 204:
                       #      status = True
                             #os.remove("".join(host_file))
                       #   else:
                       #      err_value.append(" ".join(list_dir))
                       #      status = False

                       else:
                          status = True

                 except Exception:
                     e = sys.exc_info()[0]
                     status = False
                     err_value.append(" ".join(list_dir))
                     pass

          infile.closed 
          if status is True:
               logfile = open(data[0], 'w')
               logfile.write('')
               logfile.close()

               log_err = open(data[0], 'a')
               for err_write in range(len(err_value)):
                   log_err.write(err_value[err_write]+"\n")
               log_err.close()

               status = False
               print("All file has been distributed "+data[1])
               sys.stdout.flush()

          else:
               print("Some file not distributed "+data[1])
               sys.stdout.flush()
               pass



"""
1 Byte = 8 Bit
1 Kilobyte = 1024 Bytes
1 Megabyte = 1048576 Bytes
1 Gigabyte = 1073741824 Bytes
1 Terabyte = 1099511627776 Bytes
"""

if __name__ == "__main__":
   jobs = []
   p = {}
   value = 0
   dates = time.strftime("%Y%m%d", time.localtime())
   get_lock('rsync_monitoring')
   #f = [f for f in os.listdir(log_dir) if not f.startswith('webio-replicate.log') if not f.startswith('webio-replicate.err')  if not f.endswith('.pid') and f.endswith(dates)]
   try:
         f = [f for f in os.listdir(log_dir) if not f.startswith('webio-replicate') and f.endswith(dates)]
         convert2bytes = Convert2Bytes(sys.argv[1])
         value = convert2bytes.convert2Bytes()
         print(f)
         sys.stdout.flush()

         for i in range(len(f)):
               first_hostname = f[i].rsplit('err-', 1)[1]
               end_hostname = first_hostname.rsplit(':', 1)[0]
               end_blacklist = first_hostname.rsplit('.'+dates,1)[0]

               file_dir = log_dir+f[i]
               try:

                   if os.path.getsize(file_dir) <= value:
                      queue.put((file_dir,f[i],end_blacklist,end_hostname))
                      p[i] = {'queue_host': end_hostname ,'proc': multiprocessing.Process(target=handle_queue, args=(queue,end_hostname))}
                      jobs.append(p[i]['proc'])
                      p[i]['proc'].start()

                   else:
                      print('File to large '+file_dir)
                      pass

               except Exception:
                   e = sys.exc_info()[0]
                   sendAlert(e)         

         time.sleep(90)
         for j in jobs:
               j.terminate()
               j.join()
   except Exception:
      print("""
            Please running in max log file.
               1 Byte = 8 Bit
               1 Kilobyte = 1024 Bytes
               1 Megabyte = 1048576 Bytes

           Example ./rsync.py 17MB
           """)

